# Configuration List

## COM_01

The TSG provides both Multipart and IDSCPv2. Multipart is created by means using the Information Model Java library integrated in Apache Camel, HTTPS can be configured via Camel or alternatively via Kubernetes Ingress configuration. IDSCPv2 is provided by means of the IDSCP Java library which is also integrated via Camel. Both endpoints require DATs for ingress and egress communication.

Alternatively to Kubernetes deployments, TLS enforcement can be configured using a Nginx reverse proxy within the same Docker network to act as an intermediary. See `tsg-config/proxy.conf` for an example configuration for an Nginx reverse proxy with TLS offloading.

> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#inter-connector-api

## COM_02

The TSG requires DATs for all communication with external connectors. On each connection (both ingress as egress) a DAT is required, for frequent communication DATs may be reused.

> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#camel-daps-processor

## COM_03

The TSG is by default configured with only TLSv1.3 support. Optionally configuration can be provided to use TLSv1.2 with a specified list of ciphers for compatibility with older systems.

For Docker-compose deployments, a reverse proxy can be used to configure TLS versions and ciphers. An example Nginx configuration is provided in `tsg-config/proxy.conf`, with TLSv1.2 and TLSv1.3 support with the following set of ciphers: `ECDHE-ECDSA-AES128-GCM-SHA256`,`ECDHE-RSA-AES128-GCM-SHA256`,`ECDHE-ECDSA-AES256-GCM-SHA384`,`ECDHE-RSA-AES256-GCM-SHA384`,`ECDHE-ECDSA-CHACHA20-POLY1305`,`ECDHE-RSA-CHACHA20-POLY1305`,`DHE-RSA-AES128-GCM-SHA256`,`DHE-RSA-AES256-GCM-SHA384`.

> See also (for Kubernetes deployments):  
> https://tno-tsg.gitlab.io/docs/deployment/kubernetes/

## USC_01

The TSG uses the Information Model Java library to ensure policies are formatted in the IDS Information Model. Support is included to provide policy offers attached to resources in the self description.

> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/pef/#ids-policy-language

## USC_02

The TSG allows contract negotiation based on offers defined with the offered resources or on separate offers not directly attached to a single resource. Negotiation is implemented naively, meaning that only negotiations are accepted that exactly match the offer or have a more strict version of the offer. All negotiations that provide a different structure of a contract request will be rejected, similarly when certain fields are included or omitted that are not expected.

> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/pef/#negotiation

## INF_01

The TSG by default exposes the selfdescription on the /seldescription endpoint of the connector. By default the endpoint requires an DescriptionRequest message, configurable is a "simple" GET method for browsing the selfdescription with for instance a browser.

> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#http-mime-multipart

## INF_02

The TSG ensures the selfdescription it exposes by means of the Information Model Java Library with SHACL.

> See also:  
> https://tno-tsg.gitlab.io/docs/communication/ids-messages/  
> https://tno-tsg.gitlab.io/docs/communication/message-flows/

## INF_03

The selfdescription contains at least: connector ID, title, description, endpoints (min 1), IDS Infomodel version (outbound & inbound), curator, maintainer, security profile, public key information, and the provided resources. 

## INF_04

The administration API allows for sending Description Request messages to other connectors, by default the result is parsed with the Information Model to validate the result.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#self-description

## INF_05

The TSG injects a DAT for all egress communication and requires a DAT for all ingress communication. For ingress communication the validated result is shared across the Camel route for internal usage.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#camel-daps-processor

## IAM_01

The TSG uses the proposed scheme of "{{SKI}}:keyid:{{AKI}}" for all communication with DAPS instances.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#ids-token-manager

## IAM_02

The TSG performs a check on startup validating the clock skew with the "pool.ntp.org" servers. If the clock skew is too large the connector will stop. Since the connector is aimed at running in cloud environments where modifying the clock is often not feasible from within an application.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#security-evaluation

## IAM_03

The TSG leverages the built in Java Security OCSP validation. This can be turned off by setting the environment variable `ENABLE_OCSP` to "false" (defaults to "true"). The `/healthcheck` endpoint will give you information on whether OCSP is enabled or disabled.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#security-evaluation

## IAM_04

The TSG has support for requesting the "transportCertsSha256" additional claims and additional claims that are provided by external connectors can be used in the policy enforcement framework.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#spring-configuration-properties

## BRK_01

The TSG has support for Broker inquiries, with support for both the Testbed metadata broker and the broker based on the TSG.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#self-description

## BRK_02

The TSG has support for automatic registration at a broker, with automatic updates in case there are changes of the selfdescription. Additionally the administration API offers functionality to manually register at a broker.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#self-description

## BRK_03

The TSG has support for automatic registration at a broker, with automatic updates in case there are changes of the selfdescription. Additionally the administration API offers functionality to manually update the registration at a broker.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#self-description

## BRK_04

The TSG will automatically deregister itself from the broker when it will shutdown. Additionaly the administration API offers functionality to manually deregister from a broker.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#self-description

## OS_01

The administration API allows for orchestration of related containers by means of a connection to a Kubernetes API.

For Docker-compose deployments, orchestration of containers is not supported due to security considerations. Since having access to the Docker API effectively results in root access to the host machine.

> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#workflow-management

## AUD_01

The TSG logs all control decisions it has taken with the relevant context to the STDOUT of the connector. As well as to an in-memory storage with linked signatures, which can be accessed via the Administrative API.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#ids-clearing

## AUD_02

The TSG logs all data access with the relevant context to the STDOUT of the connector. As well as to an in-memory storage with linked signatures, which can be accessed via the Administrative API.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#ids-clearing

## AUD_03

The TSG logs all interactions (except GETs) of the administration API with the relevant context to the STDOUT of the connector. As well as to an in-memory storage with linked signatures, which can be accessed via the Administrative API.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#ids-clearing

## CR_1_1

The TSG only supports human users on the administrative UI. Users must login with a username and password which results in a temporary token that can be used to interact with the API. For each user a set of roles must be provided, that limits the allowed actions of that user.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#security
> https://tno-tsg.gitlab.io/docs/core-container/configuration/

## CR_1_1_1

For each human user a separate user account must be created. Actions executed by a user are consistently logged with the users context included.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#security

## CR_1_2

Communication with internal components (e.g. pods within the Kubernetes cluster or Docker containers in the same Docker network) follows primarily API key structures. Alternatively administrators can decide to enable mutual TLS on their cluster to improve the internal identification/authentication. Identification towards external components is only supported by means of DATs.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#security

## CR_1_2_1

The TSG is intended to only communicate with external systems, unique identification and authentication is handled via DATs,.

> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#camel-daps-processor

## CR_1_3

The TSG has an embedded user management solution. Account management happens via the administrative API by users with the correct roles (ROLE_ADMIN) linked to their account. These users can create, add, disable (by removing all roles of the user), modify, or remove accounts.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#authentication-management

## CR_1_4

The TSG supports management of key identifiers primarily at startup. Identifiers of the connector can not be modified after startup of the connector due to security reasons. The key material linked to the identity can be updated, resulting in a changed identifier of the certificate ("{{SKI}}:keyid:{{AKI}}") but the linked identifier in the self description must remain the same.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/

## CR_1_5

The TSG assumes initial authenticator configuration to be present in configuration. If no initial user is provided, a user is generated automatically with credentials provided in STDOUT with the warning that this must be changed. All configuration of passwords in the configuration is required to be encrypted via BCrypt to ensure the safety of the password.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/

## CR_1_7 

The TSG offers password based authentication. The password are encoded with bcrypt, so password strength cannot be checked. 


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/

## CR_1_8

The TSG uses public key infrastructures (PKIs). The public PKIs that are used are CA/Browser Forum. At this point, the utilized company/component-specific PKIs don't follow commonly accepted best practices, because that would break interoperability with the IDS Testbed.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#security-evaluation

## CR_1_9

The components of the TSG that use public key based authentication support the check of a certificate's validity based on provided signatures. Next to this, the validation can lead back to root/leaf certificates that are deployed onto the component. Furthermore, the revocation status can be checked, also with OCSP. The PKIs ensure the user's control of the corresponding private key and they allow checking the requested destination against subject name, common name or distinguished name. Lastly, the PKIs ensure usage of algorithms which conform with internationally recognized recommendations.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#security-evaluation  
> https://tno-tsg.gitlab.io/docs/deployment/#tls-encryption

## CR_1_10

The TSG always uses a password field for passwords, meaning they will never be displayed in the browser. Also, from the error it will not be made clear what is wrong because all requests have an equal length and the same error message is shown no matter what input data was wrong.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#security

## CR_1_11

The TSG has a rate limiter on unsuccessful login attempts. A user can try 10 times in 15 minutes, after which the user has to wait 15 minutes to try again. These numbers are default, but can also be configured.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#security

## CR_1_12

N/A

## CR_1_14

N/A

## CR_2_1

The TSG has an authorization mechanism that only provides certain information to certain users which have a certain role. This configuration can be changed by using the management API. This API is only reachable for administrators of the component.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#security

## CR_2_2

N/A


## CR_2_5

The TSG has a User Interface which has basic authentication which expires after 60 minutes. Then the user has to login again. 


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#security

## CR_2_8

The TSG logs all auditable events relevant to the following categories: a) access control; b) request errors; c) control system events; d) backup and restore event; e) configuration changes f) audit log events. They always contain a timestamp, a source, a category, a type, an event ID and an event result.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#ids-clearing

## CR_2_9

The TSG provides the capability to allocate audit record storage capacity and provides mechanisms to protect against failures of the component by rotating the logs. Primarily via Kubernetes standard out handling.

For Docker-compose deployments, the audit records are stored via the Docker daemon logging driver. For more security, the Docker daemon can be configured using the journald logging driver.

> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#ids-clearing

## CR_2_10

The TSG provides the capability to protect against the loss of essential services and functions by using the Kubernetes logging solution. Optionally this can be extended to a centralized logging solution. The TSG also protects against breaches in data security and has implemented appropriate mechanisms in case of an audit processing failure. Processes are terminated in case of audit processing failures.

For Docker-compsoe deployments, the logging is similar to Kubernetes, where the default `local` or `journald` logging driver might be used or a centralized logging solution like `gelf` or `fluentd`.

> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#ids-clearing

## CR_2_11 

The audit records that are available are using timestamps according to the ISO 8601 format.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#ids-clearing

## CR_2_12

The TSG offers the capability to determine which user took a particular action. The actions are logged in the Core Container logs.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/#ids-clearing

## CR_3_1

The TSG provides the capability to protect integrity of transmitted information during communication. This is done by using TLS 1.3, the cipher suites and ports are configurable.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#security-evaluation

## CR_3_1_1 

The TSG provides the capability to verify the authenticity of received information during communication. This is done by using TLS 1.3, the cipher suites and ports are configurable.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#security-evaluation

## CR_3_3

The TSG provides the capability to support verification of the intended operation of security functions according to IEC 62443-3-3 SR3.3.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#security-evaluation

## CR_3_4

The TSG provides the capability to perform or support integrity checks on software. This is done directly in the deployment. The configuration can be found and is reported, not stored.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#security-evaluation

## CR_3_5 

The API of the TSG is protected with input validation.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#admin-api

## CR_3_6

N/A

## CR_3_7 

The TSG uses an Industrial Automation and Control System which ensures the information from error messages cannot be exploited. The TSG uses IDS Information Model Error messages to ensure this.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#inter-connector-api

## CR_3_8

The API interface is secured by authentication. This authentication is session based, meaning that the sessions can be invalidated, generated with a unique identifier based on a commonly accepted source of randomness.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#security

## CR_4_1

In the TSG, both the information at rest and the information in transit are encrypted. The information at rest is encrypted using AES, and the information in transit is encrypted using TLS. See the section about Cipher suites (COM_03) for which algorithms are used for the encryption in transit.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#security-evaluation  
> Especially the `encryptionAtRest` and `routesUsingTLS` & `allExternalAccessUrlHttps` properties.

## CR_4_2_1

If Kubernetes is setup correctly, the component should be protected against unauthorized and unintended information transfer via volatile shared memory resources.

For Docker-compose deployments, containers are isolated via Linux Control Groups that limit access to the containers memory.

Since docker containers have volatile memory by default, all restarts or shutdowns of the containers will result in clearing the volatile memory. In case of a user logout, the session is invalidated which means no one can access the session data in memory. With the next restart or shutdown, the information will be gone.


> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/

## CR_4_3 

The crytography security mechanisms that are used are RSA, AES and Bcrypt.

Passwords in configuration are always stored in Bcrypt encoding, for encryption at rest AES is used, and for TLS either RSA or ECC is used.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#spring-configuration-properties  
> https://tno-tsg.gitlab.io/docs/core-container/configuration/#security-evaluation

## CR_5_1

The TSG supports a segmentation of networks into zones and conduits by using Kubernetes network policies.

For Docker-compose deployments, segmentation of networks can be achieved by leveraging Docker networks, which only allow communication within a single network. Containers that require to cover two (or more) networks can be attached to multiple networks.

Each network has the same mechanisms for protection and monitoring as the default configuration. See the specific documentation of either Kubernetes Network policies or Docker networks on how to configure the communication in case of a segmented network setup. 


> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/kubernetes/#network-policies

## CR_6_1

The user wanting to access audit logs should have READ access on the audit logs before being able to access them.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#clearing-controller

## CR_7_1

The essential functions are always able to be accessed for the maintainer of the component. This requires access outside of Kubernetes ingresses, which can be done via for instance Kubernetes port-forwarding.

For Docker-compose deployments, a separate `socat` container can be used to temporarily expose a port of a service, for example to reach the Core Container UI outside of the reverse proxy:
```
# Retrieve internal IP address of the UI container
ip=$(docker inspect testbed-tsg-core-container-ui-1 | jq -r '.[0].NetworkSettings.Networks.testbed_local.IPAddress')
# Run a socat image to forward the UI
docker run --rm --name ui-port-mapper --network testbed_local -p 38080:80 alpine/socat TCP-LISTEN:80,fork TCP-CONNECT:$ip:80
```

> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/kubernetes/#rate-limiting
> https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/

## CR_7_2

The TSG uses ingress rate limiting and custom rate limiting for certain endpoints.

For Docker-compose deployments, rate limiting can be configured in a Nginx reverse proxy. By adding `limit_req_zone` and `limit_req` directives, see `tsg-config/proxy.conf` for example values.

> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/kubernetes/#rate-limiting

## CR_7_3

By default, the TSG uses Persistent Volume Claims from Kubernetes, these can be backed up as often as the user requires.

For Docker-compose deployments, the `tsg-data` volume can be backed up in different ways. The easiest method is by using the [Volumes Backup & Share extension](https://hub.docker.com/extensions/docker/volumes-backup-extension) for backup and restore of the volumes.

> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/kubernetes/#persistent-volume-back-ups

## CR_7_4

The TSG is deployed via Helm, meaning that a deployment can always be rolled back to a known secure state after any disruption or failure. Most of the times, in case of a Pod crash this will happen automatically, due to the nature of Deployments in Kubernetes. In case this is not enough, helm ensures that certain deployments can be rolled back to easily. By default, this rollback only happens for the services, so the data is not affected. This results in no loss of information.

For Docker-compose deployments, the containers are restarted to a secure state. The restart policy should be set to either `always`, `on-failure`, or `unless-stopped`. In most situations `unless-stopped` is the most suitable setting.

> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/

## CR_7_6

The TSG is able to be configured according to recommended network and security configurations by using Kubernetes Network Policies.

For Docker-compose deployments, network separation should be used to isolate services on the host or cluster.

Additionally, the [Kubernetes CIS Benchmark](https://www.aquasec.com/cloud-native-academy/kubernetes-in-production/kubernetes-cis-benchmark-best-practices-in-brief/) or [Docker CIS Benchmark](https://www.aquasec.com/cloud-native-academy/docker-container/docker-cis-benchmark/) can be used to validate the hardening of the operational environment to ensure security compliance.

> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/kubernetes/#network-policies

## CR_7_7

The TSG only allows to communicate with the ports that are necessary for communication. All the ports that are opened have a Kubernetes service. The Kubernetes services are all reachable within the Kubernetes cluster. If a service needs to be reachable for the outside too, an Ingress is created. This is only done for the API and the routes ports (8082 & 8080 respectively).

For Docker-compose deployments, limiting access from internal services (inside the same Docker host/cluster) can be achieved with custom networks. Limiting access from external services is normally handled by using reverse proxies, which means that no ports should be exposed by the main containers but only from the reverse proxy.

> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/#ports

## NDR_1_6

N/A


## NDR_1_13

The TSG logs all the requests made to it, also the NGINX ingress controller has a list of logs regarding all requests that are made to the components in the Kubernetes cluster. The methods of access are also controlled via ratelimiting and firewalls.

For Docker-compose deployments, access from untrusted networks must flow through a reverse proxy. This reverse proxy can be used to monitor, and block using a ModSecurity firewall, incoming requests from untrusted networks.


> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/kubernetes/#ingress-controller--cert-manager-configuration
> https://tno-tsg.gitlab.io/docs/deployment/kubernetes/#firewalling
> https://tno-tsg.gitlab.io/docs/core-container/#ids-clearing

## NDR_2_4

N/A

## NDR_3_2

The TSG protects from malicious code by using input validation on the API endpoints.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#admin-api
> https://tno-tsg.gitlab.io/docs/core-container/api/#inter-connector-api

## NDR_3_10

The TSG is able to be updated and upgraded by updating the Kubernetes deployments.

For Docker-compose deployments, the services can be updated and upgraded by updating the relevant images in the `docker-compose.yaml` file.

> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/

## NDR_3_14

N/A

## NDR_5_2

The connector is connected to the public internet either via an NGINX Ingress Controller in Kubernetes, or an NGINX reverse proxy on Docker-compose deployments. The logs of these components can be assessed to monitor communication at zone boundaries and specific ModSecurity rules, opening/closing of ports can be done to control the communication at the zone boundaries.

## NDR_5_3

There is a firewall enabled on the ingress controller which prevents the use of general purpose and person-to-person messages.

For Docker-compose deployments, a ModSecurity firewall can be activated on the Nginx reverse proxy. An example configuration of the relevant rules can be found in the `tsg-config/proxy.conf` file in the `modsecurity_rules` directives.

> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/kubernetes/#firewalling

## D_AD_1

In the architecture document of the TSG documentation, there is a architecture description of all the subcomponents of the TSG.


> See also:  
> https://tno-tsg.gitlab.io/pages/architecture/  
> https://tno-tsg.gitlab.io/docs/core-container/

## D_AD_2

All the endpoints are secured by requiring either basic authentication or an API key. This ensures the component cannot be tampered with easily.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#security

## D_AD_3

All the endpoints are secured by requiring either basic authentication or an API key. This ensures the component cannot be tampered with easily.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#security

## D_IS_1 

In the documentation of the API, there is an interface specification of all the interfaces available in the core container.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/

## D_IS_2

In the documentation of the API, there is an interface specification of all the interfaces available in the core container. This specification also includes possible parameters and default values.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/

## D_DD_1

In the architecture document of the TSG documentation, there is a architecture description of all the subcomponents of the TSG.


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/

## G_AP_1

Starting from release 1.1.2, each release has a SHA hash which a user can verify before installing the component, to make sure they install the component as it was built from the code that is open source.


> See also:  
> https://gitlab.com/tno-tsg/core-container/-/releases

## G_AP_2

In the open source documentation, there are multiple installation procedures defined. The main one is the kubernetes deployment, but there is also an option to deploy with docker compose.


> See also:  
> https://tno-tsg.gitlab.io/docs/deployment/  
> https://tno-tsg.gitlab.io/playground/overview/


## G_OG_1

The documentation regarding the API contains descriptions of which user role is allowed to view what parts of the core container in the UI. 


> See also:  
> https://tno-tsg.gitlab.io/docs/core-container/api/#security

## G_OG_2

The TSG has one mode of operation.

## S_CM_1

The TSG works with releases according to the semantic versioning scheme. This means that every release has a unique number.


> See also:  
> https://gitlab.com/tno-tsg/core-container/-/releases

## S_CM_2

The component is called Core Container, which is the core part of the TNO Security Gateway (TSG).


> See also:  
> https://tno-tsg.gitlab.io/

## S_CM_6_1

This criterion is met by the configuration list you are reading now.


## S_CM_7

The TSG works with releases according to the semantic versioning scheme. This means that every release has a unique number.


> See also:  
> https://gitlab.com/tno-tsg/core-container/-/releases

## S_CM_8

Every commit is trackable to a user in the Gitlab environment, therefore it is always known who implemented what piece of code.


> See also:  
> https://gitlab.com/tno-tsg/core-container/-/commits/master

## S_DL_1

Starting from release 1.1.2, each release has a SHA hash which a user can verify before installing the component, to make sure they install the component as it was built from the code that is open source.


> See also:  
> https://gitlab.com/tno-tsg/core-container/-/releases

## S_FR_1

In the README of the core container, a section is dedicated to dealing with security flaws. The release can also be added to the security report.


> See also:  
> https://gitlab.com/tno-tsg/core-container#how-to-report-issues

## S_FR_2

In the README of the core container, a section is dedicated to dealing with security flaw. Each security report is linked to a specific CWE or CVE instance. They should also always be linked to a specific component and how they are affected. They will also always get a label from Low to Critical, indicating the severity of the security flaw. 


> See also:  
> https://gitlab.com/tno-tsg/core-container#how-to-report-issues

## S_FR_3

In the README of the core container, a section is dedicated to dealing with security flaws. In this section, it is described how each stage of finding the correction is reported to the team.


> See also:  
> https://gitlab.com/tno-tsg/core-container#how-to-report-issues

## T_CA_1 & T_CA_2

In this repository, in the "Test-Plan" folder, more information can be found around these criteria.

## T_TD_1 & T_TD_2 & T_TD_3

In this repository, in the "Test-Plan" folder, more information can be found around these criteria.
