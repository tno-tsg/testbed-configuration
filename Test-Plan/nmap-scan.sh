#!/bin/sh

# Execute nmap with the "ssl-enum-ciphers" script to verify the TLS protocols and ciphers for services on ports 8082,8083,8088
# Grep is used to count the number of occurrences of "least strength: " that do not contain A
result=$(nmap --script ssl-enum-ciphers -p 8082,8083,8088 localhost | grep -c -e 'least strength: [^A]')

# Test whether no services have least strength lower than A
if test "$result" -ne "0"
then
  echo "[FAIL] Not all services have a least strength of A"
else 
  echo "[SUCCESS] All services have at least strength A"
fi
