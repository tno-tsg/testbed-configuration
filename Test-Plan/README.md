# Test Plan

## Notes

The tests mentioning Postman refer to the `Test Plan.postman_collection.json` collection. Please note that you need to sign in using the Sign In call for most tests. This call will set an access token in a variable, so the user does not have to copy and paste the access token.

NMAP scanning is provided in the `nmap-scan.sh` script, which either has to be `chmod`'ed with the executable flag or executed with an compatible shell.

Tests that are tested automatically can be run as follows:
```
./gradlew clean assemble ciTest
```
Or open IntellIJ on the linked file and use the IntelliJ runner to test the specific test.

## COM_01 Protected connection

Egress communication protection is tested via Postman under folder "COM_01 Protected Connection". Ingress communication protection can be tested using nmap, execute the nmap-checks.sh shell script to validate the various options. An example command is:
```
sh nmap-scan.sh
```

IDSCPv2 is tested using https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-main/src/test/kotlin/nl/tno/ids/main/IDSCPv2Test.kt. The IDSCP library itself is provided via the IDSA Github environment. HTTP MIME Multipart is primarily tested at the camel processing part at https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/camel-multipart-processor/src/test/kotlin/nl/tno/ids/camel/multipart.

## COM_02 Mutual authentication

The DAT injection and verification is tested at the Camel processor level at https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/camel-daps-processor/src/test/kotlin/nl/tno/ids/camel/daps/DapsProcessorsTest.kt. And the request and verification of the DATs is tested at https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-token-manager/src/test/kotlin/nl/tno/ids/tokenmanager.

https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-test-extensions/src/main/kotlin/nl/tno/ids/testextensions.

## COM_03 State of the art cryptography

Egress communication protection is tested via Postman under folder "COM_03 State of the art cryptograhpy". Ingress communication protection can be tested using nmap, execute the nmap-scan.sh shell script to validate the various options.

An example command is:
```
sh nmap-scan.sh
```

## USC_01 Definition of usage policies

The supported policy classes are tested inside https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-pef/src/test/kotlin/nl/tno/ids/pef. While in https://gitlab.com/tno-tsg/core-container/-/blob/1.1.2/ids-main/src/test/kotlin/nl/tno/ids/main/ArtifactProcessorTest.kt the definition of a contract offer alongside an artifact is tested.

## USC_02 Sending of usage policies

The supported negotiation sequence and capabilities of exchanging contracts are tested inside https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-pef/src/test/kotlin/nl/tno/ids/pef. While in https://gitlab.com/tno-tsg/core-container/-/blob/1.1.2/ids-main/src/test/kotlin/nl/tno/ids/main/ArtifactProcessorTest.kt the definition of a contract offer alongside an artifact is tested.

## INF_01 Self-Description (at Connector)

The selfdescription functionality is tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-selfdescription/src/test/kotlin/nl/tno/ids/selfdescription.

## INF_02 Validity of Self-Descriptions

The validity of the self-description is inherently tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-selfdescription/src/test/kotlin/nl/tno/ids/selfdescription. Due to the fact that the self description is passing the SHACL validation of the IDS Information Model Library.

## INF_03 Self-Description content

The validity of the self-description is inherently tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-selfdescription/src/test/kotlin/nl/tno/ids/selfdescription. Due to the fact that the connector won't start if the required information is not present.

## INF_04 Self-Description evaluation

All requests are flowing through the Camel Multipart processors, also incoming self descriptions. In this processor conformancy against the Information Model including SHACL shapes is checked, which is tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/camel-multipart-processor/src/test/kotlin/nl/tno/ids/camel/multipart.

## INF_05 Dynamic attribute tokens

The functionality for sending and checking DATs is built in into the Camel processors related to the DAPS. One processor will inject a DAT in all egress messages and one processor will verify all ingress messages on a valid DAT, this is tested at https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/camel-daps-processor/src/test/kotlin/nl/tno/ids/camel/daps/DapsProcessorsTest.kt.

## IAM_01 Connector identifier

The identifier of the connector for the DAPS is automatically generated from the used certificate, this behaviour is tested at https://gitlab.com/tno-tsg/core-container/-/blob/1.1.2/ids-token-manager/src/test/kotlin/nl/tno/ids/tokenmanager/ClientAssertionGeneratorTest.kt.

## IAM_02 Time Service

This feature is inherently tested in all tests that startup the Spring context from the ids-main package. Since the NTP validation is executed as a bean, this is tested in https://gitlab.com/tno-tsg/core-container/-/blob/1.1.2/ids-main/src/test/kotlin/nl/tno/ids/main/SecurityTest.kt.

## IAM_03 Online certificate status check

OCSP Stapling can't be automatically tested. But support can be built into the Postman script if a server with an incorrect OCSP configuration is provided.

## IAM_04 Attestation of dynamic attributes

The functionality for dymanic claims is tested in https://gitlab.com/tno-tsg/core-container/-/blob/1.1.2/ids-token-manager/src/test/kotlin/nl/tno/ids/tokenmanager/DynamicClaimsTest.kt.

## BRK_01 Meta Data Broker service inquiries

The Meta Data Broker interactions are tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-selfdescription.

## BRK_02 Meta Data Broker registration

The Meta Data Broker interactions are tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-selfdescription.

## BRK_03 Meta Data Broker registration update

The Meta Data Broker interactions are tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-selfdescription.

## BRK_04 Meta Data Broker deregistration

The Meta Data Broker interactions are tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-selfdescription.

## OS_01 Container support

The orchestration manager is tested against a mocked Kubernetes API in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-orchestration-manager/src/test/kotlin/nl/tno/ids/orchestrationmanager, a local test scenario is also provided but this can only be run with an active Kubernetes cluster attached to it so this test is normally disabled.

Container support on plain Docker daemons is not supported.

## AUD_01 Access control logging

The access control logging is tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-clearing/src/test/kotlin/nl/tno/ids/clearing.

## AUD_02 Data access logging

The data access logging is tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-clearing/src/test/kotlin/nl/tno/ids/clearing.

## AUD_03 Configuration changes logging

The configuration change logging is tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-clearing/src/test/kotlin/nl/tno/ids/clearing.

## CR_1_1 Human user identification and authentication

The security of the human user authentication and authorization is tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-security/src/test/kotlin/nl/tno/ids/security.

## CR_1_1_1 Unique identification and authentication

The management of the human users is tested in https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-security/src/test/kotlin/nl/tno/ids/security.

## CR_1_2 Software process and device identification and  authentication

The functionality for sending and checking DATs is built in into the Camel processors related to the DAPS. One processor will inject a DAT in all egress messages and one processor will verify all ingress messages on a valid DAT, this is tested at https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/camel-daps-processor/src/test/kotlin/nl/tno/ids/camel/daps/DapsProcessorsTest.kt.

## CR_1_2_1 Unique identification and authentication

The functionality for sending and checking DATs is built in into the Camel processors related to the DAPS. One processor will inject a DAT in all egress messages and one processor will verify all ingress messages on a valid DAT, this is tested at https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/camel-daps-processor/src/test/kotlin/nl/tno/ids/camel/daps/DapsProcessorsTest.kt.

## CR_1_3 Account management

User management testing is handled inside the Postman collection in folder "CR_1_3 Account Management", to test locking of users the request "Incorrect Login" should be executed more than 10 times within 15 minutes. Unlocking can be done after that with "Reset user".

https://gitlab.com/tno-tsg/core-container/-/blob/1.1.2/ids-main/src/test/kotlin/nl/tno/ids/main/SecurityTest.kt

## CR_1_4 Identifier management

Management of the IDS identifier is only possible on startup of the connector via configuration (in file or environment variable). Upon startup of the connector a check will be done to verify that the IDS identifier starts with either "http://" "https://" or "urn:", a sample of an incorrect configuration is provided in application.error.yaml and can be applied by modifying the volume mount in the docker-compose.yaml file.

## CR_1_5 Authenticator management

This is tested automatically, see: https://gitlab.com/tno-tsg/core-container/-/blob/1.1.2/ids-main/src/test/kotlin/nl/tno/ids/main/SecurityTest.kt.

# CR_1_7 Strength of password-based authentication

Due to the fact that the TSG uses BCrypt encoding for all passwords, this criterium can't be tested.

## CR_1_8 Public key infrastructure certificates

PKI functionality can be tested via Postman with folder "CR_1_8 Public key infrastructure certificates". With requests to insecure servers using different types of errors.

## CR_1_9 Strength of public key-based authentication

Strength of PKI can be tested via Postman with folder "CR_1_9 Strength of public key-based authentication".

## CR_1_10 Authenticator feedback

Try to login to the UI and verify that the password is not displayed in the browser. Also manually enter a wrong password and a wrong username afterwards and verify that both times the same error message is displayed, not showing what input data was wrong.

## CR_1_11 Unsuccessful login attempts

This is tested automatically, see: https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-security/src/test/kotlin/nl/tno/ids/security.

## CR_1_12 System use notification

This criterion cannot be tested as it is not applicable.

## CR_1_14 Strength of symmetric key-based authentication

This criterion cannot be tested as it is not applicable.

## CR_2_1 Authorization enforcement

This is tested automatically, see: https://gitlab.com/tno-tsg/core-container/-/blob/1.1.2/ids-main/src/test/kotlin/nl/tno/ids/main/SecurityTest.kt.

## CR_2_2 Wireless use control

This criterion cannot be tested as it is not applicable.

## CR_2_5 Session lock

Login to the UI, wait 60 minutes and try to interact with the UI. Verify that the session is expired.

## CR_2_8 Auditable events

Send a request to the TSG and view the logs to see that the request is logged in the logs of the TSG.

## CR_2_9 Audit storage capacity

Send a request to the TSG and view the logs to see that the request is logged in the logs of the TSG.

## CR_2_10 Response to audit processing failures

This is tested automatically, see: https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-clearing/src/test/kotlin/nl/tno/ids/clearing.

# CR_2_11 Timestamps Audit record

Timestamps can be verified using Postman, in folder "CR_2_11 Timestamps".

## CR_2_12 Non-repudiation

By filtering the standard out of the TSG with filter "Configuration Interaction:", all audit logs are visible with the responsible user.

## CR_3_1 Communication integrity

Execute nmap-scan.sh to execute an nmap scan for ssl ciphers, the script will print failure or success depending on whether any services have a least strenght of A.

An example command is:
```
sh nmap-scan.sh
```

## CR_3_1_1 Communication authentication

A combination of the nmap scan of CR_3_1 and the postman folders for COM_01, CR_1_8, CR_1_9 can be used to validate the communication authentication.

An example command for the nmap scan is:
```
sh nmap-scan.sh
```

## CR_3_3 Security functionality verification

This criterion cannot be tested since it is about documentation.

## CR_3_4 Software and information integrity

This can be tested by trying to deploy a connector with an erroneous application.error.yaml.

## CR_3_5 Input validation

The input validation of the API can be tested with Postman in the folder "CR_3_5 Input Validation".

## CR_3_6 Deterministic output

This criterion cannot be tested as it is not applicable.

## CR_3_7 Error handling

This criterion can be tested by looking at the source code and verify that the TSG uses IDS Information Model Error messages.

## CR_3_8 Session integrity

This is tested automatically, see: https://gitlab.com/tno-tsg/core-container/-/blob/1.1.2/ids-main/src/test/kotlin/nl/tno/ids/main/SecurityTest.kt.

## CR_4_1 Information confidentiality

This is tested automatically, see: https://gitlab.com/tno-tsg/core-container/-/tree/1.1.2/ids-artifact-manager/src/test/kotlin/nl/tno/ids/artifacts.

## CR_4_2_1 Erase of shared memory resources

This criterion cannot be tested easily, since it requires  knowledge about the setup of the deployment.

## CR_4_3  Use of cryptography

This criterion cannot be tested as it answers a question which security mechanisms are used.

## CR_5_1 Network segmentation

Deploy the TSG with a segmentation of networks into zones and conduits by using Kubernetes network policies.

For Docker-compose deployments, create separate Docker networks for different services and validate the inability to communicate between two containers in different networks. This assumes that the ports are not exposed to the host system.

## CR_6_1 Audit log accessibility

The audit log accessibility can be tested via Postman in folder "CR_6_1 Audit log accessibility".

## CR_7_1 Denial of service protection

Visit a component in OpenLens and verify the service works without opening the service to the outside world via an Ingress.

## CR_7_2 Resource management

This is tested automatically, see: https://gitlab.com/tno-tsg/core-container/-/blob/1.1.2/ids-security/src/test/kotlin/nl/tno/ids/security/AuthManagerTest.kt. 

## CR_7_3 Control system backup

The testing of backups of PVCs is not in scope of the certification. It is known that PVCs can be backed up (see https://kubernetes.io/docs/concepts/storage/volume-snapshots/).
For Docker-compose deployments, backing up volumes is not in scope of certification.

## CR_7_4 Control system recovery and reconstitution

We crashed a pod deliberately and found that the Kubernetes deployment controller fixed the pod.
For Docker-compose deployments, assuming they are started with a restart policy of always, the Docker container was stopped and the Docker daemon restarted the container.

## CR_7_6 Network and security configuration settings

We viewed the Kubernetes Network Policies and confirmed that the TSG is configured according to these recommended network and security configurations.

Additionally the [Kubernetes CIS Benchmark](https://www.aquasec.com/cloud-native-academy/kubernetes-in-production/kubernetes-cis-benchmark-best-practices-in-brief/) or [Docker CIS Benchmark](https://www.aquasec.com/cloud-native-academy/docker-container/docker-cis-benchmark/) can be used to validate the security configuration of the environment.

## CR_7_7 Least functionality

This criterion cannot be tested as it concerns answers to which ports are opened.

## NDR_1_6 Wireless Access Management

This criterion cannot be tested as it is not applicable.

## NDR_1_13 Access via untrusted networks

Send a request to the TSG and view the logs to see that the request is logged in both the logs of the TSG and the NGINX Ingress controller.

## NDR_2_4 Mobile code

This criterion cannot be tested as it is not applicable.

## NDR_3_2 Protection from malicious code

The protection from malicious code can be tested via Postman in folder "NDR_3_2 Protection from malicous code".

## NDR_3_10 Support for updates

This criterion cannot be tested, it is simply true or false.

## NDR_3_14 Integrity of the boot process

This criterion cannot be tested as it is not applicable.

## NDR_5_2 Zone boundary protection

This criterion cannot be tested as it is not applicable.

## NDR_5_3 General purpose, person-to-person communication restrictions

This criterion is not automatically tested since the required/support zones and their boundaries are implementation-specific.

## D_AD_1 Secure initialisation

This criterion cannot be tested since it is about documentation.

## D_AD_2 Tamper protection

This criterion cannot be tested since it is about documentation.

## D_AD_3 Security-enforcing mechanisms

This criterion cannot be tested since it is about documentation.

## D_IS_1 Interface purpose and usage

This criterion cannot be tested since it is about documentation.

## D_IS_2 Interface parameters

This criterion cannot be tested since it is about documentation.

## D_DD_1 Subsystem structure

This criterion cannot be tested since it is about documentation.

## G_AP_1 Acceptance procedures

This criterion cannot be tested since it is about documentation.

## G_AP_2 Installation procedures

This criterion cannot be tested since it is about documentation.

## G_OG_1 Interface usage for each user role

This criterion cannot be tested since it is about documentation.

## G_OG_2 Possible modes of operation

This criterion cannot be tested since it is about documentation.

## S_CM_1 Unique component reference

This criterion cannot be tested since it is about documentation.

## S_CM_2 Consistent usage of component reference

This criterion cannot be tested since it is about documentation.

## S_CM_6_1 Configuration list content (1)

This criterion cannot be tested since it is about documentation.

## S_CM_7 Unique identification based on configuration list

This criterion cannot be tested since it is about documentation.

## S_CM_8 Developer Information

This criterion cannot be tested since it is about documentation.

## S_DL_1 Secure delivery

This criterion cannot be tested since it is about documentation.

## S_FR_1 Tracking of reported security flaws

This criterion cannot be tested since it is about documentation.

## S_FR_2 Security flaw description

This criterion cannot be tested since it is about documentation.

## S_FR_3 Status of corrective measures

This criterion cannot be tested since it is about documentation.

## T_CA_1 Test coverage analysis

Run the tests with jacoco and you can obtain a Test coverage report.
The command is as follows:
```
./gradlew ciTest jacocoTestReport
```

This creates an exec file (in ids-main/build/jacoco/ciTest.exec), which you can run with IntellIJ to view the report.

## T_CA_2 Test procedures for subsystems

This criterion cannot be tested since it is about documentation.

## T_TD_1 Test documentation

This criterion cannot be tested since it is about documentation.

## T_TD_2 Test configuration

This criterion cannot be tested since it is about documentation.

## T_TD_3 Ordering Dependencies

This criterion cannot be tested since it is about documentation.