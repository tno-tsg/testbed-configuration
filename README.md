# Testbed configuration TSG

This repository contains all necessary files and describes the steps that need to be taken into account when setting up the testbed with the TSG. 

## Certificate creation
To create a certificate, make sure to use the CN tsg-connector.

Create a CA chain with the CA certificates and the individual connectors:
```
cat ./CertificateAuthority/data/*/*.crt > ./CertificateAuthority/data/chain.crt
```
> _Note_: the individual connectors are required to circumvent the OCSP checks executed in the connector due to the CA structure of the Testbed.

## Copy resources

Copy the folder `tsg-config` to the root of the Testbed.
Replace the Postman collection and environment files in the Testbed with the versions in this repository.

## Docker compose
The docker compose file needs to be modified by adding the following attributes:

```yaml
  tsg-core-container-server:
    cap_add:
      - SYS_TIME
    image: docker.nexus.dataspac.es/core-container:1.2.3@sha256:55a1f5a4a711a9359fb1ced5d2be7fd49ae839cd150978566dddb93555dcab36
    pull_policy: always
    restart: unless-stopped
    volumes:
      - ./tsg-config/application.yaml:/config/application.yaml
      - ./CertificateAuthority/data/cert/tsg-connector.crt:/secrets/idsidentity/ids.crt
      - ./CertificateAuthority/data/cert/tsg-connector.key:/secrets/idsidentity/ids.key
      - ./CertificateAuthority/data/chain.crt:/secrets/idsidentity/ca.crt
      - tsg-data:/resources
    networks:
      - local

  tsg-core-container-ui:
    image: docker.nexus.dataspac.es/ui/core-container-ui:1.6.8@sha256:ce5685aec0db9ccc063bf7267e442a83e4993dc7bf1b77bbafff3be94ea27b19
    pull_policy: always
    restart: unless-stopped
    environment:
      - API_BACKEND=tsg-core-container-server:8082
      - PASSWORD_LENGTH=12
      - APIKEY_LENGTH=20
    networks:
      - local

  tsg-core-container:
    image: owasp/modsecurity-crs:3.3.5-nginx-alpine-202310170110@sha256:a9fbf4283c5edcc8c2fb867cd38f948f0269726160dde170a793a30d176e4669
    restart: unless-stopped
    ports:
      - 8082:8082 # API port
      - 8083:8080 # IDS Multipart port
      - 8088:8088 # UI port
    environment:
      - PROXY=1
    volumes:
      - ./tsg-config/error.html:/usr/share/nginx/html/error.html
      - ./tsg-config/proxy.conf:/etc/nginx/conf.d/proxy.conf
      - ./CertificateAuthority/data/cert/tsg-connector.crt:/secrets/idsidentity/ids.crt
      - ./CertificateAuthority/data/cert/tsg-connector.key:/secrets/idsidentity/ids.key
      - ./CertificateAuthority/data/chain.crt:/secrets/idsidentity/ca.crt
    networks:
      - local
```

The volumes should be modified so tsg-data is added, like so:

```yaml 
volumes:
  broker-fuseki: {}
  connector-dataa: {}
  connector-datab: {}
  tsg-data: {}
```

## Postman collection
The postman collection changed a bit, because not all the calls are the same for the TSG as they are for the DSC. The headings below correspond to the folders of the Postman collection. This only highlights the differences, if a folder is not found in the headings below, the calls are similar.


## Set-up
The testsuite for the TSG consists of an init, which has a Signin call which returns an access token and saves it in a variable. This access token is needed to communicate with the TSG. 

The Set-up of the TSG is quite a bit shorter than the setup of the DSC, because almost all of the setup steps are automated when uploading an artifact. The artifact, title, description and contractOffer are sent to the connector, and afterwards a Catalog with an artifact and an offer will be created.

## Data Usage Control
Because the calls towards a resource are also handled differently in the TSG, the offers can be retrieved via only 1 call instead of 3 seperate calls in Data Usage Control. 

## Teardown
The teardown is also simplified, because by deleting an artifact, all corresponding contracts & offers will be deleted as well.